# README #

This is a simple webservice to show schedule job process.

Data are fetched via Schedule-API.

This web page already be dockerized.

### Libraries ###

#### OS ####
* CentOS 7

#### Orchestration ###
* Ansible
* Docker
* Docker-Compose

#### Services ####
* NodeJS
* Bootstrap

### Processing ###

* Ansible Playbook for installing the Docker
* Docker-Compose file for creating a service container


## Jenkins pipeline ##
### Structure ###


           Webservice Server (Docker)
                   ^
                   |
            Jenkins Server                                                                                         
                   ^
                   |
             Git Repository

### Jenkins Pipeline workflow ###

       O --> Pull Repo From Git Repo (Using Git Plugin) --> Install Docker and Docker-Compose evironment (Using Ansible plugin with vault password) --> Running Docker Compose files (Execute shell)
 
 Option: "Stop and set the build to 'failed' status if there are errors when processing a result file"
    
### Jenkins Plugins ###
* SSH Agent
* Git Plugin
* Ansible Plugin

### Repository ###

https://bitbucket.org/TinTranVan/absolute-api
 