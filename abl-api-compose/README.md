# README #

### Quick overview ###

This is a simple web page to show scheduled jobs data, such as job name, schedule, last execution, next execution.

Data are fetched via API
 

### Libraries ###

* NodeJS
* EJS 
* Bootstrap

### Configuration file ###
Config in .env file
```xml
module.exports = {
	HTTP_PORT: 5000,
	RUNDECK_ACCESS_TOKEN: 'XXXXXXXXXXXXXXX',
	RUNDECK_API_BASE_URL: 'http://127.0.0.1:8080/api/21/'
}
```

### Run with docker-compose file

Edit enviroment

Config env in .env file or in docker-compose file
```
docker-compose up . --build -d
```

### Run with docker-run
```
docker build . --tag local/rundeck-schedule-monitoring

docker-run -itd --name=rundeck-schedule-monitoring -p 8080:8080 \
-e RUNDECK_ACCESS_TOKEN='topsecret' \
-e RUNDECK_API_BASE_URL='https://127.0.0.1/api/21/' \
local/schedule-monitoring-service

```
