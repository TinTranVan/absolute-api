#!/bin/bash

HTTP_PORT=8080
APP_PATH="/usr/src/app"
ENV_PATH="/${APP_PATH}/.env"

sed -i -e "s/^HTTP_PORT.*/HTTP_PORT=${HTTP_PORT}/g" ${ENV_PATH}

if [ ! -z "${RUNDECK_ACCESS_TOKEN}" ];then
    sed -i -e "s/^RUNDECK_ACCESS_TOKEN=.*/RUNDECK_ACCESS_TOKEN=${RUNDECK_ACCESS_TOKEN}/g" ${ENV_PATH}
fi

if [ ! -z "${RUNDECK_API_BASE_URL}" ];then
    sed -i -e "s#^RUNDECK_API_BASE_URL=.*#RUNDECK_API_BASE_URL='${RUNDECK_API_BASE_URL}'#g"  ${ENV_PATH}
fi

if [ ! -z "${TIMEZONE}" ];then
    sed -i -e "s/^TIMEZONE=.*/TIMEZONE='${TIMEZONE}'/g"  ${ENV_PATH}
fi

cd ${APP_PATH}
sudo npm start